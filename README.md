# Spotify-Go

Pure Go Spotify daemon and cli

# Thanks

Thanks to Paul Liétar (https://github.com/plietar) for his doumentation and rust implimentation of the Spotify protocol in https://github.com/plietar/librespot, couldn't have even started on this with out your insight

# Disclaimer

Using this code to connect to Spotify's API is probably forbidden by them. Use at your own risk.

# License

Everything in this repository is licensed under the MIT license.
